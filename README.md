# Análisis e Interpretación de Datos



## Actividad grupal

Datasets
- **autos-electricos.csv** : Contiene información de la venta de autos eléctricos, según su tecnología, para los años 2019 a 2022.
- **marcas.csv**: Contiene información de la venta de autos de combustión y eléctricos, por marca y año.
- **actividad2.r**: Script R con algunas instrucciones para mostrar la información en gráficas y para crear un Dataframe con información estadística.